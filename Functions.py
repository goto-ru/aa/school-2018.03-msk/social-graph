def dfs(v, cols, curcol, g):
    cols[v] = curcol
    for elem in g[v]:
        if cols[elem] == -1:
            dfs(elem, cols, curcol, g)


def change_pr(v0, g, ranks, pluses):
    d = 0.85
    for elem in g[v0]:
        if len(g[elem]) != 0:
            plus = ranks[elem] / len(g[elem])
        pluses[v0] += d * plus

def kargerMinCut(graph, cuts):
    while len(graph) > 2:
        v = random.choice(list(graph.keys()))
        w = random.choice(graph[v])
        contract(graph, v, w)   
    mincut = len(graph[list(graph.keys())[0]])
    cuts.append(mincut)

def contract(graph, v, w):
    for node in graph[w]:
        if node != v:
            graph[v].append((node, 1))
        graph[node].remove(w)
        if node != v:
            graph[node].append(v)

    del graph[w]  # delete the absorbed vertex 'w'
import requests
from .Functions import dfs, change_pr


class SocialAnalyser:
    users = {}
    graph = {}
    common_stats = {'People': 0, 'Friendships': 0, 'AveragePower': 0, 'MaxPower': 0, 'MaxPowerIds': [], 'MinPower': 0,
                    'MinPowerIds': []}
    used = {}
    ranks = {}
    pages = {}
    playlists = {}
    name = ''

    def __init__(self, group_id):
        self.group_id = str(group_id)
        self.users_url = 'https://api.vk.com/method/groups.getMembers?group_id={}&offset={}&fields=sex,bdate,city&count={}&v=5.73'
        self.group_len = requests.get(self.users_url.format(self.group_id, 0, 10), verify=False).json()['response'][
            'count']
        self.friends_url = 'https://api.vk.com/method/friends.get?user_id={}&v=5.73'
        self.pluss = {}
        self.name = 'https://vk.com/club' + self.group_id

    def get_users(self):
        counter = self.group_len // 1000
        module = self.group_len % 1000
        for i in range(counter):
            cururl = self.users_url.format(self.group_id, i * 1000, 1000)
            resp = requests.get(cururl, verify=False).json()['response']['items']
            for elem in resp:
                curid = elem['id']
                if 'city' in elem.keys():
                    elem['city'] = elem['city']['title']
                self.users[curid] = elem
        cururl = self.users_url.format(self.group_id, counter * 1000, module)
        resp = requests.get(cururl, verify=False).json()['response']['items']
        for elem in resp:
            curid = elem['id']
            if 'city' in elem.keys():
                elem['city'] = elem['city']['title']
            self.users[curid] = elem
        for elem in self.users.keys():
            cururl = self.friends_url.format(elem)
            resp = requests.get(cururl, verify=False).json()
            if 'response' in resp.keys():
                resp = resp['response']['items']
            resp = [x for x in resp if x in self.users.keys()]
            self.graph[elem] = resp
        for elem in self.users.keys():
            self.used[elem] = -1
            self.pluss[elem] = 0
            self.ranks[elem] = len(self.graph[elem])
        usr = list(self.users.keys())[0]

    def count_common_stats(self, maxids=10, minids=10):
        self.common_stats['People'] = self.group_len
        self.common_stats['Friendships'] = sum([len(x) for x in self.graph.values()]) // 2
        self.common_stats['AveragePower'] = (self.common_stats['Friendships'] * 2) / self.common_stats['People']
        self.common_stats['MaxPower'] = max([len(x) for x in self.graph.values()])
        self.common_stats['MinPower'] = min([len(x) for x in self.graph.values()])
        self.common_stats['MaxPowerIds'] = [y[0] for y in
                                            list(sorted(self.graph.items(), key=lambda x: len(x[1]), reverse=True))[
                                            :minids]]
        self.common_stats['MinPowerIds'] = [y[0] for y in
                                            list(sorted(self.graph.items(), key=lambda x: len(x[1])))[:minids]]

    def find_components(self):
        curcolor = 0
        while any(x == -1 for x in self.used.values()):
            start = -1
            for elem in self.used.items():
                if elem[1] == -1:
                    start = elem[0]
                    break
            dfs(start, self.used, curcolor, self.graph)
            curcolor += 1

    def count_page_rank(self):
        for i in range(10):
            for elem in self.pluss.keys():
                self.pluss[elem] = 0
            for elem in self.users.keys():
                change_pr(elem, self.graph, self.ranks, self.pluss)
            for elem in self.ranks.keys():
                self.ranks[elem] += self.pluss[elem]

    def find_common_pages(self):
        urilka = 'https://api.vk.com/method/users.getSubscriptions?user_id={}&v=5.73'
        for elem in self.users.keys():
            resp = requests.get(urilka.format(elem), verify=False).json()
            if 'response' in resp.keys():
                resp = resp['response']['groups']['items']
            else:
                resp = []
            for elem2 in resp:
                if elem2 in self.pages.keys():
                    self.pages[elem2].append(elem)
                else:
                    self.pages[elem2] = [elem]
        self.pages = list(sorted(self.pages.items(), key=lambda x: len(x[1]), reverse=True))

    def present_pr(self, file=None):
        ret = []
        i = 1
        for key in self.ranks:
            helpdict = {}
            helpdict['x'] = self.users[key].get('first_name', '') + ' ' + self.users[key].get('last_name', '')
            helpdict['y'] = self.ranks[key]
            ret.append(helpdict)
            i += 1
        return ret

    def find_clusters(self):
        pass


def get_ptl(id):
    ptl = SocialAnalyser(id)
    ptl.get_users()
    ptl.count_common_stats()
    ptl.find_components()
    ptl.count_page_rank()
    ptl.find_common_pages()
    lst = ptl.present_pr()
    return ptl, lst

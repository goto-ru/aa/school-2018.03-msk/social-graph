def dfs(v, cols, curcol, g):
    cols[v] = curcol
    for elem in g[v]:
        if cols[elem] == -1:
            dfs(elem, cols, curcol, g)


def change_pr(v0, g, ranks, pluses):
    d = 0.85
    for elem in g[v0]:
        if len(g[elem]) != 0:
            plus = ranks[elem] / len(g[elem])
            pluses[v0] += d * plus

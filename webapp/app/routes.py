from flask import render_template, request
from flask.json import jsonify

from app import app
from .social import get_ptl
import json
import pickle
import os


@app.route('/', methods=['GET', 'POST'])
def request_id():
    id = request.args.get('id')
    return render_template('homepage.html', id=id)


@app.route('/ajax/get_graph/')
def get_graph():
    id = request.args.get('id')
    fname = 'data%s.pkl' % id
    print(fname)

    if os.path.isfile(fname):
        with open(fname, 'rb') as f:
            arr = pickle.load(f)
            ptl = arr[0]
            lst = arr[1]

    else:
        ptl, lst = get_ptl(id)
        with open(fname, 'wb') as f:
            pickle.dump([ptl, lst], f)
    request_data = {'most_popular': ptl.common_stats['AveragePower'],
                    'count': len(ptl.used),
                    }



    request_data['data'] = lst
    return jsonify(request_data)
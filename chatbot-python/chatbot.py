#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from config import token
from XO import wins, cires, new_cities
import random

digits = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
synons = {'хорошо': ['прекрас', 'хорош', 'здоров', 'клас', 'клев', 'великолеп', 'чуд', 'клёв', 'отличн'], 'норм': ['норм', 'ничего', 'справн', 'ладн', 'ок', 'неплох', 'обычн', 'как всегда', 'поряд'], 'плохо': ['плох', 'отврат', 'хуж', 'худ', 'хре', 'дурн', 'неважн', 'сквер', 'незавидн', 'негодн', 'мерз', 'пакост', 'ужас']}
greetvar = ['привет', 'здорово', 'хай', 'Приветствую', 'Здравствуйте', 'Здравствуй']
moodvar = ['Как настроение?', 'Как дела?', 'Что с настроением?', 'Как настрой?']
numsql = {2: 'двух', 3: 'трёх', 4: 'четырех', 5: 'пяти', 6: 'шести', 7: 'семи', 8: 'восьми', 9: 'девяти', 10: 'десяти'}

class Chatbot():
    def __init__(self):
        self.current_state = '<GREETINGS>'
        self.extra_message = ''
        self.user_turn = ''
        self.user_choice = ''
        self.bot_choice = ''
        self.board = '123\n456\n789'
        self.num_piles = 0
        self.piles = []
        self.used = ['']
        self.user_letter = ''

    def understand(self, message):
        if self.current_state == '<GREETINGS>':
            self.current_state = '<MOOD_ASKING>'
        elif self.current_state == '<MOOD_ASKING>':
            if any(x in message.lower() for x in synons['хорошо']):
                self.current_state = '<PLAYINGCT>'
            elif any(x in message.lower() for x in synons['норм']):
                self.current_state = '<PLAYINGNM>'
            elif any(x in message.lower() for x in synons['плохо']):
                self.current_state = '<PLAYINGXO>'
            else:
                self.extra_message = 'Что-то настроение у тебя невнятное. Повтори попонятнее, пожалуйста.'
        else:
            self.user_turn = message + ''
        
                
            

    def generate(self):
        if self.current_state == '<GREETINGS>':
            return random.choice(greetvar)
        elif self.current_state == '<MOOD_ASKING>':
            if not self.extra_message:
                return random.choice(moodvar)
            return self.extra_message
        elif self.current_state == '<PLAYINGXO>':
            if self.user_turn == '':
                text = 'Ну как же так. Давай взбодримся и сыграем в крестики-нолики\nТы играешь, ставя '
                varrs = ['X', 'O']
                self.user_choice = random.choice(varrs)
                del varrs[varrs.index(self.user_choice)]
                self.bot_choice = varrs[0]
                return text + self.user_choice + '\nТвой ход(напиши номер клеточки, в которую хочешь поставить):\n123\n456\n789'
            else:
                ind = self.board.index(self.user_turn)
                self.board = self.board[:ind] + self.user_choice + self.board[ind + 1:]
                if wins(self.board) == self.user_choice:
                    self.current_state = '<MOOD_ASKING>'
                    self.user_turn = ''
                    return 'Ты выиграл! Молодец! Ну что, как теперь настроение?'
                elif wins(self.board) == self.bot_choice:
                    self.current_state = '<MOOD_ASKING>'
                    self.user_turn = ''
                    return 'Я выиграл! Ну что, как теперь настроение?'
                else:
                    if all(x in 'XO' for x in ''.join(self.board.split('\n'))):
                        self.current_state = '<MOOD_ASKING>'
                        self.user_turn = ''
                        return 'Что ж, ничья! Ну что, как теперь настроение?'
                    indi = 0
                    for elem in self.board.split('\n'):
                        for elem2 in elem:
                            if elem2 in digits:
                                indi = elem2
                                break
                    nindi = self.board.index(indi)
                    self.board = self.board[:nindi] + self.bot_choice + self.board[nindi + 1:]
                    return 'Я поставил в ' + indi + '\n' + 'Твой ход(напиши номер клеточки, в которую хочешь поставить):\n' + self.board
        elif self.current_state == '<PLAYINGNM>':
            if not self.user_turn:
                self.num_piles = random.randint(2, 10)
                text = 'Чтобы твое настроение стало получше, давай сыграем в ним на ' + numsql[self.num_piles] + ' кучках\n'
                text += 'Вот эти кучки:\n'
                self.piles = [random.randint(1, 20) for i in range(self.num_piles)]
                text += ' '.join([str(x) for x in self.piles]) + '\n'
                text += 'Твой ход. Напиши номер кучки, из которой хочешь взять, и количество камней.'
                return text
            else:
                self.user_turn = self.user_turn.split()
                self.piles[int(self.user_turn[0]) - 1] -= int(self.user_turn[1])
                if all(x == 0 for x in self.piles):
                    self.current_state = '<MOOD_ASKING>'
                    self.user_turn = ''
                    return 'Ты выиграл! Молодец! Что теперь с настроением?'
                bot_turn_pile = random.randint(1, self.num_piles - 1)
                while self.piles[bot_turn_pile - 1] == 0:
                    bot_turn_pile = random.randint(1, self.num_piles - 1)
                bot_turn_num = random.randint(1, self.piles[bot_turn_pile - 1])
                self.piles[bot_turn_pile - 1] -= bot_turn_num
                tetext = 'Я взял ' + str(bot_turn_num) + ' из кучки №' + str(bot_turn_pile) + '. Твой ход\n'
                if all(x == 0 for x in self.piles):
                    self.current_state = '<MOOD_ASKING>'
                    self.user_turn = ''
                    return tetext[:-11] + '\nЯ выиграл! Надеюсь, ты не расстроился! Что теперь с настроением?'
                return tetext + ' '.join([str(x) for x in self.piles])
        else:
            if not self.user_turn:
                return 'Тогда ты, наверное, готов сыграть в города! Начинай! Есл захочешь сдаться, напиши "сдаюсь"'
            else:
                if self.user_turn == 'сдаюсь':
                    self.current_state = '<MOOD_ASKING>'
                    return 'Я выиграл! Ну что, как теперь настроение?'
                self.user_turn = self.user_turn.capitalize()
                if self.user_letter == self.user_turn[0] and self.user_turn in cires:
                    used.append(self.user_turn)
                    if all(x in used for x in new_cities[self.user_turn[-1].upper()]):
                        return 'Я сдаюсь! Ты выиграл! Молодец!'
                    bot_turn = random.choice(new_cities[self.user_turn[-1].upper()])
                    while bot_turn in used:
                        bot_turn = random.choice(new_cities[self.user_turn[-1].upper()])
                    self.user_letter = bot_turn[-1].upper()
                    return bot_turn + ' .Твой ход'
                elif self.user_letter != self.user_turn[0]:
                    return 'Не на ту букву! Попробуй еще раз'
                else:
                    return 'Я не знаю такого города! Попробуй еще раз'

    def process(self, message):
        self.understand(message) 
        return self.generate()


    def process_tg(self, bot, update):
        update.message.reply_text(self.process(update.message.text))

    def error(self, bot, update, error):
        print('Update "%s" caused error "%s"' % (update, error))

    def start_tg(self, bot, update):
        print(self.generate())
        update.message.reply_text(self.generate())

    def run(self, tg=True):
        if tg:
            updater = Updater(token)
            dp = updater.dispatcher
            dp.add_handler(CommandHandler("start", self.start_tg))
            dp.add_handler(MessageHandler(Filters.text, self.process_tg))

            dp.add_error_handler(self.error)
            updater.start_polling()

            print("Tg started")

            updater.idle()
        while True:
            print(self.generate())
            self.understand(input().strip())



if __name__ == '__main__':
    chat = Chatbot()
    chat.run(False)

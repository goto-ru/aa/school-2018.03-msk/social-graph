#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

import random

class States:
    GREETING = '<GREETING>'
    ERROR = '<ERROR>'

class Chatbot():
    "Our chatbot"
    
    state = States.ERROR
    hello = ['привет', 'прив', 'здравствуй', 'здравствуйте', 'добрый день', 'доброе утро', 'добрый вечер', 'доброго времени суток', 'приветствую', 'приветствую тебя', 'приветствую вас', 'мое почтение', 'наше почтение', 'hello', 'hey', 'heeey', 'hi']
    answers = []
    grammar = {
        States.GREETING: ['<HELLO> <FRIEND>',],
        '<HELLO>': ['Hello,', 'Hey,', 'Hi,', 'Здравствуй,', 'Здравствуйте,', 'Добрый день,', 'Доброе утро,', 'Добрый вечер,', 'Доброго времени суток,', 'Приветствую,', 'Приветствую тебя,', 'Приветствую вас,', 'Мое почтение,', 'Наше почтение,', 'Heeey,',],
        '<FRIEND>': ['друг', 'friend', 'гость'],
        
        States.ERROR: ['Я тебя не понимаю('],
    }

    def understand(self, message):
        for hi in self.hello:
            if hi in message.lower():
                self.state = States.GREETING
                return
        self.state = States.ERROR

    def generate_by_nterm(self, n_term):
        response = random.choice(self.grammar[n_term])
        message = ''
        for sym in response.split(' '):
            if sym[0] != '<' and sym[-1] != '>':
                message += sym
            else:
                message += self.generate_by_nterm(sym)
            message += ' '
        
        return message

    def generate(self):
        return self.generate_by_nterm(self.state)

    def process(self, message):
        self.understand(message) 
        return self.generate()

    def process_tg(self, bot, update):
        update.message.reply_text(self.process(update.message.text))

    def error(self, bot, update, error):
        print('Update "%s" caused error "%s"' % (update, error))

    def start_tg(self,bot, update):
        update.message.reply_text(self. generate())

    def run(self, tg=True):
        """Start the bot."""
        if tg:
            updater = Updater("592142708:AAG5BpZ7vG7GFJFTOFfc5vIZrcDxJLYR7kQ")
            dp = updater.dispatcher
            dp.add_handler(CommandHandler("start", self.start_tg))
            dp.add_handler(MessageHandler(Filters.text, self.process_tg))

            dp.add_error_handler(self.error)
            updater.start_polling()

            print("Tg started")

            updater.idle()
        while True:
            print(self.generate())
            self.understand(input().strip())



if __name__ == '__main__':
    chat = Chatbot()
    chat.run(True)
